from django.conf.urls import url, include
from django.contrib import admin
# admin.autodiscover()

urlpatterns = [
    # To allow admin section
    url('', include('blogengine.urls')),
    url(r'^admin/', include(admin.site.urls)),
]
